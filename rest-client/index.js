"use strict";
exports.__esModule = true;
var express = require("express");
var soap = require("soap");
var bodyParser = require("body-parser");
var cors = require("cors");
var xml2js_1 = require("xml2js");
var user_url = 'http://localhost:8000/user/service';
var wallet_url = 'http://localhost:8000/wallet/service';
// Create a new express app instance
var app = express();
app.use(cors());
app.use(bodyParser.json());
var postEntities = function (req, res, next) {
    if (req.method === 'POST' && !req.body) {
        res.status(400);
        res.send({
            error: true,
            message: 'No se han enviado parámetros'
        });
    }
    else {
        next();
    }
};
app.use(postEntities);
var parseError = function (body, res) {
    xml2js_1.parseString(body, function (err, result) {
        res.status(400).send({
            'error': true,
            'code': result['SOAP-ENV:Envelope']['SOAP-ENV:Body'][0]['SOAP-ENV:Fault'][0]['faultcode'],
            'message': result['SOAP-ENV:Envelope']['SOAP-ENV:Body'][0]['SOAP-ENV:Fault'][0]['faultstring']
        });
    });
};
app.post('/user/auth', function (req, res) {
    var args = {
        email: req.body.email,
        password: req.body.password
    };
    soap.createClient(user_url + "?wsdl", function (err, client) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
        }
        else {
            client.auth(args, function (err, result) {
                if (!err) {
                    console.log(result);
                    res.status(200).send({
                        'error': false,
                        'status': result["return"].item[0].value.$value,
                        'token': result["return"].item[1].value.$value
                    });
                }
                else {
                    parseError(result.body, res);
                }
            });
        }
    });
});
app.post('/user/signup', function (req, res) {
    var args = {
        email: req.body.email,
        password: req.body.password,
        name: req.body.name,
        dni: req.body.dni,
        phone: req.body.phone
    };
    soap.createClient(user_url + "?wsdl", function (err, client) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
        }
        else {
            client.signup(args, function (err, result) {
                if (!err) {
                    res.status(200).send({
                        'error': false,
                        'status': result["return"].item.value.$value
                    });
                }
                else {
                    parseError(result.body, res);
                }
            });
        }
    });
});
app.post('/user/validate', function (req, res) {
    var args = {
        token: req.body.token
    };
    soap.createClient(user_url + "?wsdl", function (err, client) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
        }
        else {
            client.validate(args, function (err, result) {
                if (!err) {
                    res.status(200).send({
                        'error': false,
                        'status': result["return"].item.value.$value
                    });
                }
                else {
                    parseError(result.body, res);
                }
            });
        }
    });
});
app.post('/wallet/balance', function (req, res) {
    var args = {
        token: req.body.token
    };
    soap.createClient(wallet_url + "?wsdl", function (err, client) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
        }
        else {
            client.getBalance(args, function (err, result) {
                if (!err) {
                    res.status(200).send({
                        'error': false,
                        'status': result["return"].item[0].value.$value,
                        'amount': result["return"].item[1].value.$value
                    });
                }
                else {
                    parseError(result.body, res);
                }
            });
        }
    });
});
app.post('/wallet/charge', function (req, res) {
    var args = {
        userToken: req.body.token,
        dni: req.body.dni,
        phone: req.body.phone,
        amount: req.body.amount
    };
    soap.createClient(wallet_url + "?wsdl", function (err, client) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
        }
        else {
            client.chargeBalance(args, function (err, result) {
                if (!err) {
                    res.status(200).send({
                        'error': false,
                        'status': result["return"].item[0].value.$value,
                        'message': result["return"].item[1].value.$value
                    });
                }
                else {
                    parseError(result.body, res);
                }
            });
        }
    });
});
app.post('/wallet/pay', function (req, res) {
    var args = {
        token: req.body.token,
        amount: req.body.amount
    };
    soap.createClient(wallet_url + "?wsdl", function (err, client) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
        }
        else {
            client.pay(args, function (err, result) {
                if (!err) {
                    res.status(200).send({
                        'error': false,
                        'status': result["return"].item[0].value.$value,
                        'message': result["return"].item[1].value.$value
                    });
                }
                else {
                    parseError(result.body, res);
                }
            });
        }
    });
});
app.post('/wallet/confirm', function (req, res) {
    var args = {
        token: req.body.token,
        txId: req.body.txId,
        txToken: req.body.txToken
    };
    soap.createClient(wallet_url + "?wsdl", function (err, client) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
        }
        else {
            client.confirmPay(args, function (err, result) {
                if (!err) {
                    res.status(200).send({
                        'error': false,
                        'status': result["return"].item[0].value.$value,
                        'message': result["return"].item[1].value.$value
                    });
                }
                else {
                    parseError(result.body, res);
                }
            });
        }
    });
});
app.listen(8080, function () {
    console.log('App is listening on port 8080!');
});
