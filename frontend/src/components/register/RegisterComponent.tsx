import React, {FormEvent, useEffect, useState} from 'react';
import { withRouter, RouteComponentProps } from 'react-router';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Snackbar from "@material-ui/core/Snackbar";
import Grow from '@material-ui/core/Grow';
import { TransitionProps } from '@material-ui/core/transitions';

function GrowTransition(props: TransitionProps) {
    return <Grow {...props} />;
}


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    card: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    pos: {
        marginBottom: 12,
    },
    form: {
        '& > *': {
            margin: theme.spacing(1),
            width: '100%',
        },
    },
}));

type Props = RouteComponentProps & {};

const RegisterComponent = (props: Props) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [dni, setDni] = useState('');
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [message, setMessage] = useState('');
    const [openSnack, setOpenSnack] = useState(false);
    const classes = useStyles();
    useEffect(() => {
        const token = localStorage.getItem('token');

        if (token) {
            props.history.push('/dash');
        }
    }, []);

    const onSubmit = (e: FormEvent) => {
        e.preventDefault();
        e.stopPropagation();

        const data = {
            email,
            password,
            name,
            dni,
            phone
        };

        fetch(
            'http://localhost:8080/user/signup',
            {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res) {
                    if (res.error) {
                        setMessage(res.message[0]);
                        setOpenSnack(true);
                    } else {
                        setMessage('Ya puede iniciar sesión');
                        setOpenSnack(true);
                    }
                }
            })
            .catch(err => console.log(err));

    }

    const handleCloseSnack = () => {
        setOpenSnack(false)
    };

    return (
        <>
            <Grid container className={classes.root} spacing={2} justify="center">
                <Grid item xs={12} md={6} lg={3}>
                    <Card className={classes.card}>
                        <CardContent>
                            <Typography variant="h3" gutterBottom>
                                Registrarse
                            </Typography>
                            <form className={classes.form} onSubmit={onSubmit}>
                                <TextField id="outlined-email" label="Email" variant="outlined" onChange={(event) => {setEmail(event.target.value)}} required />
                                <TextField id="outlined-password" type="password" label="Password" variant="outlined" onChange={(event) => {setPassword(event.target.value)}} required />
                                <TextField id="outlined-dni" label="Dni" variant="outlined" onChange={(event) => {setDni(event.target.value)}} required />
                                <TextField id="outlined-name" label="Nombre Completo" variant="outlined" onChange={(event) => {setName(event.target.value)}} required />
                                <TextField id="outlined-phone" label="Teléfono" variant="outlined" onChange={(event) => {setPhone(event.target.value)}} required />
                                <Button variant="contained" color="primary" type="submit">
                                    Entrar
                                </Button>
                            </form>
                        </CardContent>
                        <CardActions>
                            <Link component={RouterLink} to="/">
                                Iniciar Sesión
                            </Link>
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
            <Snackbar
                open={openSnack}
                onClose={handleCloseSnack}
                autoHideDuration={2000}
                TransitionComponent={GrowTransition}
                message={message}
            />
        </>
    );
}

export default withRouter(RegisterComponent);