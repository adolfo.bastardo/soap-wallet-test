import React, {FormEvent, useEffect, useState} from 'react';
import { withRouter, RouteComponentProps } from 'react-router';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Snackbar from "@material-ui/core/Snackbar";
import Grow from '@material-ui/core/Grow';
import { TransitionProps } from '@material-ui/core/transitions';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

function GrowTransition(props: TransitionProps) {
    return <Grow {...props} />;
}


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    buttonLogout: {
        marginLeft: "auto",
        marginRight: 0
    },
    itemContent: {
        marginTop: 50,
    },
    card: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    pos: {
        marginBottom: 12,
    },
    form: {
        '& > *': {
            margin: theme.spacing(1),
            width: '100%',
        },
    },
}));

type Props = RouteComponentProps & {};

const DashboardComponent = (props: Props) => {
    const [token, setToken] = useState('');
    const [balance, setBalance] = useState(0.0);
    const [amount, setAmount] = useState(0.0);
    const [dni, setDni] = useState('');
    const [phone, setPhone] = useState('');
    const [message, setMessage] = useState('');
    const [openSnack, setOpenSnack] = useState(false);


    const [newAmount, setNewAmount] = useState(0.0);
    const [txId, setTxId] = useState(0);
    const [txToken, setTxToken] = useState('');

    const classes = useStyles();

    const getBalance = () => {
        const data = {
            token,
        };
        fetch(
            'http://localhost:8080/wallet/balance',
            {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res) {
                    if (res.error) {
                        setMessage(res.message[0]);
                        setOpenSnack(true);
                    } else {
                        setBalance(parseFloat(res.amount));
                    }
                }
            })
            .catch(err => console.log(err));
    };

    useEffect(() => {
        const tokenStr = localStorage.getItem('token');

        if (!tokenStr) {
            props.history.push('/');
        } else {
            setToken(tokenStr);
        }
    }, []);

    useEffect(() => {
        if (token) {
            getBalance();
        }
    }, [token]);

    const onSubmitCharge = (e: FormEvent) => {
        e.preventDefault();
        e.stopPropagation();

        const data = {
            token,
            dni,
            phone,
            amount
        };

        fetch(
            'http://localhost:8080/wallet/charge',
            {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res) {
                    if (res.error) {
                        setMessage(res.message[0]);
                        setOpenSnack(true);
                    } else {
                        setMessage(res.message);
                        setOpenSnack(true);
                        getBalance();
                    }
                }
            })
            .catch(err => console.log(err));

    };

    const onSubmitPay = (e: FormEvent) => {
        e.preventDefault();
        e.stopPropagation();

        const data = {
            token,
            amount: newAmount
        };

        fetch(
            'http://localhost:8080/wallet/pay',
            {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res) {
                    if (res.error) {
                        setMessage(res.message[0]);
                        setOpenSnack(true);
                    } else {
                        setMessage(res.message);
                        setOpenSnack(true);
                    }
                }
            })
            .catch(err => console.log(err));

    };

    const onSubmitConfirm = (e: FormEvent) => {
        e.preventDefault();
        e.stopPropagation();

        const data = {
            token,
            txId,
            txToken
        };

        fetch(
            'http://localhost:8080/wallet/confirm',
            {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if (res) {
                    if (res.error) {
                        setMessage(res.message[0]);
                        setOpenSnack(true);
                    } else {
                        setMessage(res.message);
                        setOpenSnack(true);
                        getBalance();
                    }
                }
            })
            .catch(err => console.log(err));

    };

    const handleCloseSnack = () => {
        setOpenSnack(false)
    };

    const logout = () => {
        setToken('');
        localStorage.removeItem('token');
        props.history.push('/');
    };

    return (
        <>
            <Grid container className={classes.root} spacing={2} alignContent="flex-start">
                <Grid item xs={12}>
                    <AppBar position="static">
                        <Toolbar>
                            <Typography variant="h3" >
                                Servicio SOAP-Rest-Front
                            </Typography>
                            <Button color="inherit" onClick={logout} className={classes.buttonLogout}>Salir</Button>
                        </Toolbar>
                    </AppBar>
                </Grid>
                <Grid item xs={12} md={6} lg={3} className={classes.itemContent}>
                    <Card className={classes.card}>
                        <CardContent>
                            <Typography variant="h5" gutterBottom>
                                Saldo en la Billetera:
                            </Typography>
                            <Typography variant="h1" gutterBottom>
                                {balance}
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12} md={6} lg={3}>
                    <Card className={classes.card}>
                        <CardContent>
                            <Typography variant="h5" gutterBottom>
                                Recargar billetera
                            </Typography>
                            <form className={classes.form} onSubmit={onSubmitCharge}>
                                <TextField id="outlined-dni" label="Tu DNI" variant="outlined" onChange={(event) => {setDni(event.target.value)}} required />
                                <TextField id="outlined-phone" label="Tu teléfono" variant="outlined" onChange={(event) => {setPhone(event.target.value)}} required />
                                <TextField id="outlined-amount" type="number" label="Cantidad" variant="outlined" onChange={(event) => {setAmount(parseFloat(event.target.value))}} required />
                                <Button variant="contained" color="primary" type="submit">
                                    Recargar
                                </Button>
                            </form>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12} md={6} lg={3} className={classes.itemContent}>
                    <Card className={classes.card}>
                        <CardContent>
                            <Typography variant="h5" gutterBottom>
                                Registrar pago:
                            </Typography>
                            <form className={classes.form} onSubmit={onSubmitPay}>
                                <TextField id="outlined-amount" type="number" label="Cantidad" variant="outlined" onChange={(event) => {setNewAmount(parseFloat(event.target.value))}} required />
                                <Button variant="contained" color="primary" type="submit">
                                    Registrar
                                </Button>
                            </form>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12} md={6} lg={3} className={classes.itemContent}>
                    <Card className={classes.card}>
                        <CardContent>
                            <Typography variant="h5" gutterBottom>
                                Confirmar pago:
                            </Typography>
                            <form className={classes.form} onSubmit={onSubmitConfirm}>
                                <TextField id="outlined-id" label="ID de la transacción" variant="outlined" onChange={(event) => {setTxId(parseInt(event.target.value, 10))}} required />
                                <TextField id="outlined-txtoken" label="Token de la transacción" variant="outlined" onChange={(event) => {setTxToken(event.target.value)}} required />
                                <Button variant="contained" color="primary" type="submit">
                                    Confirmar
                                </Button>
                            </form>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
            <Snackbar
                open={openSnack}
                onClose={handleCloseSnack}
                autoHideDuration={5000}
                TransitionComponent={GrowTransition}
                message={message}
            />
        </>
    );
}

export default withRouter(DashboardComponent);