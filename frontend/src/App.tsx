import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import './App.css';
import LoginComponent from "./components/login/LoginComponent";
import RegisterComponent from "./components/register/RegisterComponent";
import DashboardComponent from "./components/dashboard/DashboardComponent";

function App() {
  return (
      <Router>
        <Switch>
          <Route path="/" exact>
            <LoginComponent />
          </Route>
          <Route path="/signup">
            <RegisterComponent />
          </Route>
            <Route path="/dash">
                <DashboardComponent />
            </Route>
        </Switch>
      </Router>
  );
}

export default App;
