<?php


namespace App\Service;


use App\Entity\User;
use App\Entity\Wallet;
use App\Entity\Transaction;
use App\Provider\UserProvider;
use App\SoapException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use SoapFault;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class WalletService
{
    /* @var EntityManager $em */
    protected $em;

    /* @var UserPasswordEncoderInterface $em */
    protected $passwordEncoder;

    /* @var UserProvider $provider */
    protected $provider;

    /* @var Swift_Mailer $mailer */
    protected $mailer;

    /**
     * WalletService Constructor
     *
     * @param EntityManager $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param UserProvider $provider
     * @param Swift_Mailer $mailer
     */
    public function __construct(EntityManager $em, UserPasswordEncoderInterface $passwordEncoder, UserProvider $provider, Swift_Mailer $mailer)
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
        $this->provider = $provider;
        $this->mailer = $mailer;
    }

    /*
    |--------------------------------------------------------------------------
    | Public Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Recharge a wallet, returning status of true with message, or throws SoapFault.
     *
     * @param string $userToken
     * @param string $dni
     * @param string $phone
     * @param float $amount
     * @return array
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws SoapException
     */
    public function chargeBalance($userToken, $dni, $phone, $amount)
    {
        if($this->provider->validate($userToken)) {
            $user = $this->em->getRepository(User::class)->findOneBy([
               "dni" => $dni,
               "phone" => $phone,
            ]);
            if ($user && $user instanceof User) {
                $wallet = $this->em->getRepository(Wallet::class)->findOneBy([
                    "user_id" => $user->getId(),
                ]);
                if (is_null($wallet) || !($wallet instanceof Wallet)) throw new SoapException('La billetera no existe', 'SOAP-ENV:Client');
                $balance = $wallet->getBalance() + $amount;
                $wallet->setBalance($balance);
                try {
                    $this->em->flush();
                    return ['status' => 'true', 'message' => "Se ha recargado la billetera."];
                } catch (Exception $e) {
                    throw new SoapException('Error al intentar recargar la billetera', 'SOAP-ENV:Client');
                }
            } else {
                throw new SoapException('No existe el cliente.', 'SOAP-ENV:Client');
            }
        }
        throw new SoapException('El token de usuario no es válido.', 'SOAP-ENV:Client');
    }

    /**
     * Validates token, returning status of true, or throws SoapFault.
     *
     * @param string $token
     * @param float $amount
     * @return array
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws SoapException
     */
    public function pay($token, $amount)
    {
        if ($this->provider->validate($token)) {
            $user = $this->em->getRepository(User::class)->findOneBy(["token" => $token]);
            $transaction = new Transaction();
            $transaction->setUserId($user);
            $transaction->setWalletId($user->getWalletId());
            $transaction->setAmount($amount);
            $tx_token = md5(uniqid(rand(), true));

            $transaction->setToken($tx_token);
            try {
                $this->em->persist($transaction);
                $this->em->flush();

                $tx_id = $transaction->getId();
                $message = (new Swift_Message('Compra' . $tx_id . 'para la billetera SOAP TEST'))
                    ->setFrom('abastardo@liberarte.org.ve')
                    ->setTo($user->getUsername())
                    ->setBody(
                        "
                                <html>
                                    <body>
                                        <p>Este es un mesaje desde tu billetera
                                            <br>El id de la transacción es: $tx_id y el token es: $tx_token.
                                        </p>
                                    </body>
                                </html>
                            ",
                        'text/html'
                    );
                if ($this->mailer->send($message) == 0) {
                    throw new SoapException('No se pudo enviar el correo', 'SOAP-ENV:Client');
                }
                return ['status' => 'true', "message" => "Se ha enviado al correo el id y token de la transacción. Con estos datos debe validar el pago"];
            } catch (Exception $e) {
                throw new SoapException('Error al intentar pagar', 'SOAP-ENV:Client');
            }
        } else {
            throw new SoapException('El token de usuario no es válido.', 'SOAP-ENV:Client');
        }

    }

    /**
     * Confirm pay for an user, returning status of true, or throws SoapFault.
     *
     * @param string $token
     * @param int $txId
     * @param string $txToken
     * @return array
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws SoapException
     */
    public function confirmPay($token, $txId, $txToken)
    {
        if ($this->provider->validate($token)) {
            $user = $this->em->getRepository(User::class)->findOneBy(["token" => $token]);
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy([
                "id" => $txId,
                "token" => $txToken
            ]);
            if ($transaction) {
                $wallet = $user->getWalletId();
                $balance = (float)$wallet->getBalance();
                $amount = $transaction->getAmount();
                if ($balance >= $amount) {
                    $newBalance = $balance - $amount;
                    $wallet->setBalance($newBalance);
                    try {
                        $this->em->flush();
                        return ['status' => 'true', "message" => "Se ha confirmado el pago y descontado de la billetera el monto de la compra"];
                    } catch (Exception $e) {
                        throw new SoapException('Error al intentar confirmar el pago', 'SOAP-ENV:Client');
                    }
                }
                throw new SoapException('El saldo de la billetera no es suficiente.', 'SOAP-ENV:Client');
            }
            throw new SoapException('La transacción no existe.', 'SOAP-ENV:Client');
        }
        throw new SoapException('El token de usuario no es válido.', 'SOAP-ENV:Client');
    }

    /**
     * Get balance in wallet, returning status of true and amount, or throws SoapFault.
     *
     * @param string $token
     * @return array
     * @throws SoapException
     */
    public function getBalance($token)
    {
        if ($this->provider->validate($token)) {
            $user = $this->em->getRepository(User::class)->findOneBy(["token" => $token]);
            $wallet = $user->getWalletId();
            $balance = $wallet->getBalance();
            return ['status' => 'true', "amount" => $balance];
        }
        throw new SoapException('El token de usuario no es válido.', 'SOAP-ENV:Client');
    }

}