<?php


namespace App\Service;


use App\Entity\User;
use App\Entity\Wallet;
use App\Provider\UserProvider;
use App\SoapException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use SoapFault;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    /* @var EntityManager $em */
    protected $em;

    /* @var UserPasswordEncoderInterface $em */
    protected $passwordEncoder;

    /* @var UserProvider $provider */
    protected $provider;

    /**
     * UserProvider Constructor
     *
     * @param EntityManager $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param UserProvider $provider
     */
    public function __construct(EntityManager $em, UserPasswordEncoderInterface $passwordEncoder, UserProvider $provider)
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
        $this->provider = $provider;
    }

    /*
    |--------------------------------------------------------------------------
    | Public Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Authenticates user/password, returning status of true with id and token, or throws SoapFault.
     *
     * @param string $email
     * @param string $password
     * @return array
     * @throws SoapException
     */
    public function auth($email, $password)
    {
        $userToken = $this->provider->signin($email, $password);

        if (is_array($userToken)) {
            return ['status' => 'true', 'token' => $userToken['token']];
        } else {
            throw new SoapException('Credenciales incorrectas.', 'SOAP-ENV:Client');

        }

    }

    /**
     * Validates token, returning status of true, or throws SoapFault.
     *
     * @param string $token
     * @return array
     * @throws SoapException
     */
    public function validate($token)
    {

        if ($this->provider->validate($token)) {
            return ['status' => 'true'];
        } else {
            throw new SoapException('Token inválido o ha expirado.', 'SOAP-ENV:Client');

        }

    }

    /**
     * Register a new User, returning status of true, or throws SoapFault.
     *
     * @param string $email
     * @param string $password
     * @param string $name
     * @param string $dni
     * @param string $phone
     * @return array
     * @throws SoapException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function signup($email, $password, $name, $dni, $phone)
    {

        $user = new User();
        $user->setName($name);
        $user->setEmail($email);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
        $user->setDni($dni);
        $user->setPhone($phone);

        $wallet = new Wallet();
        $wallet->setUserId($user);
        $wallet->setBalance("0.00");

        try {
            $this->em->persist($user);
            $this->em->persist($wallet);
            $this->em->flush();
            return ['status' => 'true'];
        } catch (Exception $e) {
            throw new SoapException('Error al intentar guardar el usuario', 'SOAP-ENV:Client');
        } catch (UniqueConstraintViolationException $e) {
            throw new SoapException('Este email ya está registrado', 'SOAP-ENV:Client');
        }

    }

}