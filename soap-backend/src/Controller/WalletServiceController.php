<?php

namespace App\Controller;

use App\Provider\UserProvider;
use App\Service\WalletService;
use App\SoapException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use Laminas\Soap\AutoDiscover;
use Laminas\Soap\Server;
use SoapFault;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class WalletServiceController extends AbstractController
{
    private $passwordEncoder;
    private $provider;
    private $mailer;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, UserProvider $provider, Swift_Mailer $mailer)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->provider = $provider;
        $this->mailer = $mailer;
    }

    /**
     * @Route("/wallet/service", name="wallet_service")
     * @param Request $request
     */
    public function index(Request $request)
    {
        $response = new Response();
        $entityManager = $this->getDoctrine()->getManager();

        $autodiscover = new AutoDiscover();
        $autodiscover->setClass(new WalletService($entityManager, $this->passwordEncoder, $this->provider, $this->mailer))
            ->setUri('http://localhost:8000/wallet/service')
            ->setServiceName('WalletService');
        $wsdl = $autodiscover->generate();

        if ($request->server->get('REQUEST_METHOD') == 'GET') {
            if (! isset($_GET['wsdl'])) {
                $response->setStatusCode('400', 'Client Error');
                return $response;
            }

            $response->headers->set('Content-Type', 'text/xml; charset=UTF-8');

            $response->setContent($wsdl->toXml());
            return $response;
        }
        if ($request->server->get('REQUEST_METHOD') != 'POST') {
            $response->setStatusCode('400', 'Client Error');
            return $response;
        }

        // pointing to the current file here
        $soap = new Server();
        $soap->setObject(new WalletService($entityManager, $this->passwordEncoder, $this->provider, $this->mailer));
        $soap->setUri('http://localhost:8000/wallet/service');
        $soap->registerFaultException([
            SoapException::class,
            UniqueConstraintViolationException::class
        ]);
        $soap->setReturnResponse(true);

        $response_soap = $soap->handle();

        if ($response_soap instanceof SoapFault) {
            return $this->serverFault($response, $response_soap);
        }

        $response->headers->set('Content-Type', 'text/xml; charset=UTF-8');
        $response->setContent($response_soap);

        return $response;

    }

    /**
     * Return error response and log stack trace.
     *
     * @param Response $response
     * @param Exception $exception
     * @return Response
     */
    public function serverFault(Response $response, Exception $exception)
    {
        $response->setStatusCode(400);
        $response->setContent($this->renderView(
            'fault/fault.xml.twig',
            [
                "faultcode" => $exception->getCode(),
                "faultstring" => $exception->getMessage()
            ]
        ));
        return $response;
    }
}
