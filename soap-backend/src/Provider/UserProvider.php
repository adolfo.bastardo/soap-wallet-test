<?php


namespace App\Provider;

use App\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserProvider
{
    /* @var EntityManagerInterface $em */
    protected $em;

    /* @var UserPasswordEncoderInterface $em */
    protected $passwordEncoder;

    /**
     * UserProvider Constructor
     *
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Signin the user, returning token or false in case of error.
     *
     * @param string $email
     * @param string $password
     * @return array | boolean
     */
    public function signin($email, $password) {
        $user = $this->em->getRepository(User::class)->findOneBy(["email" => $email]);
        if ($user && $user instanceof User && $this->passwordEncoder->isPasswordValid($user, $password)) {
            $token = md5(uniqid(rand(), true));
            $user->setToken($token);
            $user->setTokenModifiedAt(new DateTime());

            try {
                $this->em->flush();
                return [
                    'id' => $user->getId(),
                    'token' => $token
                ];
            } catch (ORMException $e) {
                return false;
            }
        }

        return false;
    }

    /**
     * Validate the user, returning true or false in case of error.
     *
     * @param string $token
     * @return boolean
     */
    public function validate($token) {
        $user = $this->em->getRepository(User::class)->findOneBy(["token" => $token]);
        $date_now = new DateTime();

        if (!is_null($user)) {
            $modified = $user->getTokenModifiedAt();

            if ($modified && $modified instanceof DateTime) {
                $diff = $date_now->diff($modified);
                if ($diff->h < 1 || $diff->d < 1) return true;
            }
        }

        return false;
    }
}