<?php

namespace App\Entity;

use App\Repository\WalletRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WalletRepository::class)
 */
class Wallet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="wallet_id", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user_id;

    /**
     * @ORM\Column(type="float")
     */
    private $balance;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="wallet_id", orphanRemoval=true)
     */
    private $transactions_id;

    public function __construct()
    {
        $this->transactions_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(User $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getBalance(): ?float
    {
        return $this->balance;
    }

    public function setBalance(float $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactionsId(): Collection
    {
        return $this->transactions_id;
    }

    public function addTransactionsId(Transaction $transactionsId): self
    {
        if (!$this->transactions_id->contains($transactionsId)) {
            $this->transactions_id[] = $transactionsId;
            $transactionsId->setWalletId($this);
        }

        return $this;
    }

    public function removeTransactionsId(Transaction $transactionsId): self
    {
        if ($this->transactions_id->contains($transactionsId)) {
            $this->transactions_id->removeElement($transactionsId);
            // set the owning side to null (unless already changed)
            if ($transactionsId->getWalletId() === $this) {
                $transactionsId->setWalletId(null);
            }
        }

        return $this;
    }
}
