<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dni;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $token_modified_at;

    /**
     * @ORM\OneToOne(targetEntity=Wallet::class, mappedBy="user_id", cascade={"persist", "remove"})
     */
    private $wallet_id;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="user_id", orphanRemoval=true)
     */
    private $transactions_id;

    public function __construct()
    {
        $this->transactions_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getTokenModifiedAt(): ?\DateTimeInterface
    {
        return $this->token_modified_at;
    }

    public function setTokenModifiedAt(?\DateTimeInterface $token_modified_at): self
    {
        $this->token_modified_at = $token_modified_at;

        return $this;
    }

    public function getWalletId(): ?Wallet
    {
        return $this->wallet_id;
    }

    public function setWalletId(Wallet $wallet_id): self
    {
        $this->wallet_id = $wallet_id;

        // set the owning side of the relation if necessary
        if ($wallet_id->getUserId() !== $this) {
            $wallet_id->setUserId($this);
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactionsId(): Collection
    {
        return $this->transactions_id;
    }

    public function addTransactionsId(Transaction $transactionsId): self
    {
        if (!$this->transactions_id->contains($transactionsId)) {
            $this->transactions_id[] = $transactionsId;
            $transactionsId->setUserId($this);
        }

        return $this;
    }

    public function removeTransactionsId(Transaction $transactionsId): self
    {
        if ($this->transactions_id->contains($transactionsId)) {
            $this->transactions_id->removeElement($transactionsId);
            // set the owning side to null (unless already changed)
            if ($transactionsId->getUserId() === $this) {
                $transactionsId->setUserId(null);
            }
        }

        return $this;
    }
}
