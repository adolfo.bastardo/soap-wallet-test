<?php


namespace App;


use Throwable;

class SoapException extends \SoapFault
{
    public function __construct($message = "", $code = 'Sender', Throwable $previous = null) {
        parent::SoapFault($code, $message);
        $this->code = $code;
    }
}