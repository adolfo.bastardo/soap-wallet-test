# Prueba de Interfell FullStack SOAP-CLIENTE-FRONT

## Requisitos

Para poder probar el server con éxito se debe tener instalado los siguientes programas en su PC:

    - Docker
    - Docker Compose

## Pasos para levantar

Ejecutar en la terminal `docker-compose up`, esperar hasta que todos los componentes se hayan creado y configurado, luego ejecutar en otra terminal en la misma carpeta del proyecto los comandos:

    docker-compose exec myapp composer install
    docker-compose exec myapp php bin/console doctrine:migrations:migrate

En el archivo `.env` en la carpeta `soap-backend` modificar la variable `MAILER_URL` por la configuración del server smtp de su preferencia

Y con esto se configurará el server en la dirección: http://localhost:3000

Este proyecto fue creado usando los siguientes componentes:

- PHP
- Composer
- Symfony
- Laminas SOAP
- Node
- Express
- TypeScript
- Yarn
- React
- Material-UI

Se necesitan libres en el sistema los puertos: `8000`, `8080` y `3000`